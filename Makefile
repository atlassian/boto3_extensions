.PHONY: help setup test clean build check check-all

help:
	@echo "Available commands:"
	@echo "  build:        Build packages"
	@echo "  check:        Run pre-commit checks across modified files"
	@echo "  check-all:    Run pre-commit checks across all files"
	@echo "  clean:        Clean up temporary files"
	@echo "  setup:        Configure development environment"
	@echo "  test:         Run tests"

setup:
	poetry install
	petry run pre-commit install

test:
	poetry run pytest --verbose --color=yes --junitxml=test_results tests

build:
	poetry build

clean:
	rm -rf MANIFEST
	rm -rf docs
	rm -rf dist
	rm -rf .eggs .cache .coverage htmlcov dist test_results
	rm -rf *.egg-info
	find . -type d -name '__pycache__' -exec rm -rf '{}' \; || exit 0
	find . -name '*.pyc' -exec rm -rf '{}' \;

check:
	poetry run pre-commit run

check-all:
	poetry run pre-commit run --all-files
